<?php

define('NUMBER_OF_CARDS_IN_A_DECK', 52);
define('NUMBER_OF_CARD_COLORS', 4);
define('NUMBER_OF_CARDS_WITH_SAME_COLOR', 13);

define('RED', 'Red');
define('BLACK', 'Black');

define('HEARTS', 'Hearts');
define('CLUBS', 'Clubs');
define('SPADES', 'Spades');
define('DIAMONDS', 'Diamonds');

define('ACE', 1);
define('JACK', 12);
define('QUEEN', 13);
define('KING', 14);

define('ACE_STRING', 'Ace');
define('JACK_STRING', 'Jack');
define('QUEEN_STRING', 'Queen');
define('KING_STRING', 'King');