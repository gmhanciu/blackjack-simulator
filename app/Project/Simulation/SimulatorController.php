<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 11-Oct-19
 * Time: 20:04
 */

namespace App\Project\Simulator;

use App\Http\Controllers\Controller;

class SimulatorController extends Controller
{
    public function index()
    {
        return view('project::index');
    }
}