<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 12-Oct-19
 * Time: 0:46
 */

namespace App\Project;


class Transform
{
    public static function parseSimulationData($data)
    {
        foreach ($data as $key => $value)
        {
            switch ($key)
            {
                case 'numberOfDeals':
                case 'numberOfDecks':
                    $value = intval($value);
                    break;
            }
            $data[$key] = $value;
        }

        $data = collect($data)->recursive();

        return $data;
    }
}