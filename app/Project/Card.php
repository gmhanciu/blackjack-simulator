<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 12-Oct-19
 * Time: 0:52
 */

namespace App\Project;



class Card
{
    private $color;
    private $symbol;
    private $number;
    private $value;
    private $name;
    private $fullName;

    public function __construct(string $color, string $symbol, int $number, string $name)
    {
        $this->color = $color;
        $this->symbol = $symbol;
        $this->number = $number;
        $this->value = CardDefines::getCardValue($number);
        $this->name = $name;
        $this->fullName = $this->getCardFullName();
    }

    public function getCardFullName()
    {
        return $this->name . ' of ' . $this->symbol;
    }
}