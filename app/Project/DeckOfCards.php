<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 12-Oct-19
 * Time: 0:54
 */

namespace App\Project;


class DeckOfCards
{
//    private $numberOfCards = NUMBER_OF_CARDS_IN_A_DECK;
    private $cards;
    private $cardsNumbers;
    private $cardsColors;
    private $cardsSymbols;
    private $cardsColorsWithSymbols;

    public function __construct()
    {
        $cards = [];

        $this->cardsNumbers = CardDefines::getCardsNumbers();
        $this->cardsColors = CardDefines::getCardsColors();
        $this->cardsSymbols = CardDefines::getCardsSymbols();
        $this->cardsColorsWithSymbols = CardDefines::getCardsColorsWithSymbols();

        foreach ($this->cardsColors as $cardColor)
        {
            $cardsSymbolsForColor = $this->cardsColorsWithSymbols[$cardColor];
            foreach ($cardsSymbolsForColor as $cardSymbol)
            {
                foreach ($this->cardsNumbers as $cardNumber)
                {
                    $cardName = CardDefines::getCardName($cardNumber);
                    $card = new Card($cardColor, $cardSymbol, $cardNumber, $cardName);
                    $cards[] = $card;
                }
            }
        }

        $this->cards = $cards;
    }
}