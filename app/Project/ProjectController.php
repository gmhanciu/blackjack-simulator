<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 11-Oct-19
 * Time: 20:30
 */

namespace App\Project;

use App\Http\Controllers\Controller;
use App\Project\Simulation\Simulation;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        return view('project::index');
    }

    public function startSimulation(Request $request)
    {
        $data = Transform::parseSimulationData($request->all());
        return new Simulation($data);
    }
}