<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 12-Oct-19
 * Time: 1:16
 */

namespace App\Project;

class CardDefines
{
    public static function getCardValue($number)
    {
        $response = null;

        switch ($number)
        {
            case 1:
                $response = [
                    1,
                    11,
                ];
                break;
            case $number > 10:
                $response = 10;
                break;
            default:
                $response = $number;
                break;
        }

        return $response;
    }

    public static function getCardsColors()
    {
        return [
            RED,
            BLACK,
        ];
    }
    public static function getCardsSymbols()
    {
        return [
            HEARTS,
            DIAMONDS,
            SPADES,
            CLUBS
        ];
    }

    public static function getCardsColorsWithSymbols()
    {
        return [
            RED => [
                HEARTS,
                DIAMONDS,
            ],
            BLACK => [
                SPADES,
                CLUBS,
            ]
        ];
    }

    public static function getCardsNumbers()
    {
        return [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            12,
            13,
            14,
        ];
    }

    public static function getCardName($cardNumber)
    {
        $response = null;

        switch ($cardNumber)
        {
            case ACE:
                $response = ACE_STRING;
                break;
            case JACK:
                $response = JACK_STRING;
                break;
            case QUEEN:
                $response = QUEEN_STRING;
                break;
            case KING:
                $response = KING_STRING;
                break;
            default:
                $spelloutNumber = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
                $response = $spelloutNumber->format($cardNumber);
                $response = ucfirst($response);
                break;
        }

        return $response;
    }
}