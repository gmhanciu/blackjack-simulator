<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::name('simulator.')
    ->namespace('\App\Project')
    ->group(function () {
        Route::get('/', 'ProjectController@index')->name('index');
        Route::post('/start', 'ProjectController@startSimulation')->name('start');
    });
