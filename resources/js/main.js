// require('./bootstrap');
import './bootstrap';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import Vue from 'vue';
import store from './store/index';

import App from './components/App';

// import axios from 'axios';
// Vue.prototype.$axios = axios;
// Vue.prototype.$axios = window.axios;

/* eslint-disable no-new */
new Vue({
    store,
    render: h => h(App)
}).$mount('#app');

