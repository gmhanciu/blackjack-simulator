export default {
    disableInput (state, payload)
    {
        const numberOfDeals = payload.numberOfDeals;
        const numberOfDecks = payload.numberOfDecks;

        if (numberOfDeals === 0 && numberOfDecks === 0)
        {
            state.disableDealsInput = false;
            state.disableDecksInput = false;
        }
        else if (numberOfDeals === 0 && numberOfDecks !== 0)
        {
            state.disableDealsInput = true;
        }
        else if (numberOfDeals !== 0 && numberOfDecks === 0)
        {
            state.disableDecksInput = true;
        }
    }
}