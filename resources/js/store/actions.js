export default {
    disableInput ({state, commit, rootState}, payload)
    {
        return new Promise(function (resolve, reject) {
            commit('disableInput', payload);
            resolve();
        });
    },
    async startSimulation ({state, commit, rootState}, payload)
    {
        const url = route('simulator.start', payload);

        // let r = await this._vm.$axios
        let r = await window.axios
        .post(url)
        .then( response => {
            console.log(response);
            // /*
            // * check difference between payload and response.data
            // * in case the client ordered something then edu then removed all but edu, edu gets deleted too
            // * and it will be returned in the response data, but it won't be in payload
            // *
            // * based on this, we need to update the edu input value in vue
            // * */
            //
            // if (Object.keys(response.data.bySizes).length > 1)
            // {
            //
            // }
            //
            // console.log(response.data, payload);

        })
        .catch( error => {
        })
        .finally();

    }
}