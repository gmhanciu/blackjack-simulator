export default {
    getDealsInputState: (state) => {
        return state.disableDealsInput;
    },
    getDecksInputState: (state) => {
        return state.disableDecksInput;
    },
}